function CI = compress(I,L,t, method)

% Forward Wavelet Transform
[wc,s] = wavedec2(I,L,method);

% Threshold wavelet coefficients
temp = wc(1:s(1,1)*s(1,2)); 
                            %this will contain the coarsest
                            %approximation 
wc = wthresh(wc,'h',t);
wc(1:1:s(1,1)*s(1,2)) = temp;

% Count number of non-zero coefficients
nz = nnz(wc);

[N, J] = size(I);

% Inverse Wavelet Transform
A = appcoef2(wc, s, method, L);
[H, V, D] = detcoef2('all', wc, s, L);
CI = idwt2(A,H,V,D, method);

% Print out the compression ratio

disp('Compression ratio:')
disp(num2str(N*J/nz));





