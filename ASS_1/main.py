import numpy as np
import matplotlib.pyplot as plt
from scipy import signal



def mag_spectrum(psd, frequency, color):
    plt.magnitude_spectrum(psd, Fs=frequency, color=color)


def plotFigComparison(array, xt, title):
    """
    Prepares a figure for comparison of two functions. First, both original
    functions are plotted and then their Fourier Transform are plotted.
    """
    cols = 1
    rows = 3

    fig, axs = plt.subplots(rows, cols, constrained_layout=True)
    fig.suptitle(title, fontsize=12)

    axs[0].plot(xt[1], array[0], color='r')
    axs[1].plot(xt[1], array[1], color='b')
    axs[2].plot(xt[0], array[2], color='r')
    axs[2].plot(xt[0], array[3], color='b')

    """
    for ax, i in zip(axs, range(0, 4)):
        if i % 2 == 0:
            ax.plot(t, array[i], color='r')
        else:
            ax.plot(t, array[i], color='b')
    """
        #ax.xaxis.set_major_locator(plt.MultipleLocator(np.pi / 2))
        #ax.xaxis.set_minor_locator(plt.MultipleLocator(np.pi / 12))
        #ax.xaxis.set_major_formatter(plt.FuncFormatter(multiple_formatter()))

N = 1024

fs = 2 * np.pi / N


y = {}

y[0] = np.sin(fs * t)


"""
k = np.arange(-int(N / 2), int(N + 1)/2, 1)
t = 2 * np.pi / N
k = np.arange(-1, 1, t)

y = {}

y[0] = np.sin(10 * t)
y[1] = np.sin(10 * t)

y[2] = np.log(np.fft.fft(y[0]))
y[3] = np.log(np.fft.fft(y[1]))
#y[3] = (np.fft.fft(y[1]))

KT = {}
KT[0] = k
KT[1] = t

plotFigComparison(y, KT, "sin(100t) vs. sin(t), N = %d"% N)
"""
plt.show()
